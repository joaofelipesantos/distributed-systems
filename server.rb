#!/usr/bin/env ruby
require "socket"
require 'ipaddr'
require 'thread'

# Require thread pool
load 'ThreadPool.rb'

$semaphore = Mutex.new
$cv = ConditionVariable.new

# Default portnumber
portnumber = 8000

# Connect to Google to find IP
$local_ip = UDPSocket.open {|s| s.connect("64.233.187.99", 1); s.addr.last}

class Server
  def initialize( port, ip )
    @server = TCPServer.open( ip, port )
    @connections = Hash.new
    @rooms = Hash.new
    @clients = Hash.new
    @connections[:server] = @server
    @connections[:rooms] = @rooms
    @connections[:clients] = @clients
    run
  end
 
  def run
    # Default thread pool size
    #thread_pool_size = 5
    
    # Create the thread pool
    #pool = ThreadPool.new(thread_pool_size)

    loop {
      Thread.start(@server.accept) do | client |

      # Get message from client
      line = client.gets

      # System Control Messages
      # Stops the service with the KILL_SERVICE message
      if (line == "KILL_SERVICE")
        client.close
        abort("Shutdown server.")

      # Displays server information  with the HELO text message
      elsif (line[0..4] == "HELO ")
         client.puts "#{line}IP:#{local_ip}\nPort:#{portnumber}\nStudentID:#{student_id}"
      end

      # Send the time to the client
      #client.puts(Time.now.ctime)  

      # Chat Control Messages
      #if (line[0..14] != "JOIN_CHATROOM: ")
      #  puts "#{client} : Error #101 : Invalid message"
      #  client.puts("Error #101 : Invalid message")  
      #  client.close  
      #  #break

      # Valid join chat room message
      #else

      # Assign client to chatroom
      line.slice! "JOIN_CHATROOM: "
      line.slice! "\n"
      client_chatroom =  line.to_sym
      # Test if it is a valid chat room
      if client_chatroom.empty? || client_chatroom == "\n"
        puts "#{client} : ERROR_CODE: #102\nERROR_DESCRIPTION: Invalid chatroom name"
        client.puts("ERROR_CODE: #102\nERROR_DESCRIPTION: Invalid chatroom name")  
        Thread.kill self
      end

      #client_name = client.gets.chomp.to_sym
      chatroom_already_exists = false
      @connections[:rooms].each do |other_chatroom_name, other_chat|
        if client_chatroom == other_chatroom_name || client == other_chat
          chatroom_already_exists = true
        end
      end

      chatroom_port = 0

      if chatroom_already_exists == false
         puts "Creating new chatroom: #{client_chatroom}"
         chatroom_port = rand(2000..8000)
         @rooms[client_chatroom] = []
         #chatroom_server = TCPServer.open(chatroom_port)
      end
      @rooms[client_chatroom]  << client


      # Assigns client's IP
      line = client.gets
      line.slice! "CLIENT_IP: "
      line.slice! "\n"
      client_ip = line
      # Test if it is a valid chat room
      if client_ip != '0'
        if client_ip.empty? || client_ip == "\n" 
        puts "#{client} : ERROR_CODE: #103\nERROR_DESCRIPTION: Invalid IP"
        client.puts("ERROR_CODE: #103\nERROR_DESCRIPTION: Invalid IP")  
        Thread.kill self
        end
      end
         
      # Assigns client's portnumber
      line = client.gets
      line.slice! "PORT: "
      line.slice! "\n"
      client_port = line
      if client_port != 0
        if client_port.empty? || client_port == "\n"
          puts "#{client} : ERROR_CODE: #104\nERROR_DESCRIPTION: Invalid port"
          client.puts("ERROR_CODE: #104\nERROR_DESCRIPTION: Invalid port")  
          Thread.kill self
        end
      end
      
            
      # Assigns client's nickname/alias
      line = client.gets
      line.slice! "CLIENT_NAME: "
      line.slice! "\n"
      client_name = line.to_sym
      if client_name.empty? || client_name == "\n"
        puts "#{client} : ERROR_CODE: #105\nERROR_DESCRIPTION: Invalid alias"
        client.puts("ERROR_CODE: #105\nERROR_DESCRIPTION: Invalid alias")  
        Thread.kill self
      end

      #client_name = client.gets.chomp.to_sym
      @connections[:clients].each do |other_name, other_client|
        if client_name == other_name || client == other_client
          client.puts "This username already exist"
          Thread.kill self
        end
      end

      puts "#{client_name} #{client}"
      @connections[:clients][client_name] = client

    client.puts("JOINED_CHATROOM: #{client_chatroom}\nSERVER_IP: #{$local_ip}\nPORT: #{chatroom_port}\nROOM_REF: #{@rooms.keys.length}\nJOIN_ID: #{@rooms.keys.length}#{@clients.keys.length}")

      #client.puts "Connection established, Thank you for joining! Happy chatting"

      #connect_to_chatroom( client_name, client, chatroom_server)

      listen_user_messages(client_chatroom, client_name, client )
      end
    }.join

  end
 
  def listen_user_messages(chatroom, username, client )

        # Runs forever getting client messages
        loop {
          line = client.gets

          # Disconnect client procedures
          if (line[0] == "D")
            puts line
            line = client.gets
            puts line
            line = client.gets
            puts line

            client.puts("Disconnected.") 

            @clients.delete(username)
            client.close

            Thread.kill self

          elsif (line[0] == "L" )
            run

          else
            # Receive proper chat ID reference
            puts line
            line.slice! "CHAT: "
	    line.delete!("\n")
            room_ref = line

            # Receive proper client ID reference
            line = client.gets	
            puts line
            line.slice! "JOIN_ID: "
            line.delete!("\n")
            join_id = line

            # Receive proper client's name
            line = client.gets	
            puts line
            line.slice! "CLIENT_NAME: "
  	    line.delete!("\n")
            client_name = line

            # Receive proper client's message
            line = client.gets	
            puts line
            line.slice! "MESSAGE: "
            line.delete!("\n")
            msg = line
            # Trick to remove \n\n, not elegant, but for now it works
            line = client.gets
            puts line
            line = client.gets
            puts line

            template_msg = "CHAT: #{room_ref}\nCLIENT_NAME: #{client_name}\nMESSAGE: #{msg}\n\n"
            puts template_msg
            @rooms.values_at(chatroom).each do |current_clients|
              current_clients.each do |another_client|
                # Sent the messages to the right clients
                unless client == another_client
                  #another_client.puts "#{username.to_s}: #{msg}"
                  another_client.puts template_msg
                end
              end
            end
          end
        }
  end

end

# Providing Command-line Arguments
ARGV.each do|shell_argument|
  # Abort program if the user tries to provide more than 1 port number
  if (ARGV.size > 1)
    abort("Enter only one port number \nstart.sh [portnumber]")

  # Replace default port number with the user input
  elsif (ARGV.size == 1)
  portnumber = shell_argument
  puts "Port number: #{shell_argument}" 
  end
end 

Server.new( portnumber, "localhost" )

