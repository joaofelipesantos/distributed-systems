#!/usr/bin/env ruby 
require "socket"

$chatroom_details = Hash.new

class Client
  def initialize( server )
    @server = server
    @request = nil
    @response = nil
    run
    listen
    send
    @request.join
    @response.join
  end

  def run
    # Get client data
    puts "Enter the username:"
    msg_name = $stdin.gets.chomp
    puts "Enter chatroom name"
    msg_chatroom = $stdin.gets.chomp
    msg_ip = 0
    msg_port = 0

# Send data to login server
msg = "JOIN_CHATROOM: #{msg_chatroom}\nCLIENT_IP: #{msg_ip}\nPORT: #{msg_port}\nCLIENT_NAME: #{msg_name}"
@server.puts(msg)

# Recevice proper chatroom data
line = @server.gets
puts line
line.slice! "JOINED_CHATROOM: "
line.slice! "\n"
$chatroom_details["chatroom_name"] = line

# Assigns chatroom's IP
line = @server.gets	
#puts line
line.slice! "SERVER_IP: "
line.slice! "\n"
$chatroom_details["chatroom_ip"] = line

# Assigns chartoom's portnumber
line = @server.gets	
#puts line
line.slice! "PORT: "
line.slice! "\n"
$chatroom_details["chatroom_port"] = line

# Assigns chatrooms's reference
line = @server.gets	
#puts line
line.slice! "ROOM_REF: "
line.slice! "\n"
$chatroom_details["chatroom_ref"] = line

# Assigns client's id
line = @server.gets
#puts line	
line.slice! "JOIN_ID: "
line.slice! "\n"
$chatroom_details["client_id"] = line

# Assign client's name to hash
$chatroom_details["client_name"] = msg_name

# Assign chatroom's port and IP to hash
$chatroom_details["ip"] = msg_ip
$chatroom_details["port"] = msg_port

  end

  def listen
    @response = Thread.new do
      loop {
        #msg = @server.gets
        #puts "#{msg}"

        # Receive proper chat ID reference
        line = @server.gets
        
        # Disconnect client method
        if (line[0] == "D")
          puts line
          abort("Shutdown client.")
        end

        line.slice! "CHAT: "
	line.delete!("\n")
        room_ref = line

	# Receive proper client's name
        line = @server.gets	
	#puts line
	line.slice! "CLIENT_NAME: "
	line.delete!("\n")
	client_name = line

	# Receive proper client's message
	line = @server.gets
	#puts line
	line.slice! "MESSAGE: "
	line.delete!("\n")
	msg = line
        #puts msg
        line = @server.gets

        puts "[Room #{room_ref}: #{client_name}] \t #{msg}"
      }
    end
  end
 
  def send
    #puts "Enter the username:"
    @request = Thread.new do
      loop {
        msg = $stdin.gets
        #puts msg
        if msg == "%Disconnect\n"
          template_msg = "DISCONNECT: #{$chatroom_details["ip"]}\nPORT: #{$chatroom_details["port"]}\nCLIENT_NAME: #{$chatroom_details["client_name"]}" 
          @server.puts( template_msg )
  
        elsif msg == "%Leave\n"
          template_msg = "LEFT_CHATROOM: #{$chatroom_details["chatroom_ref"]}\nJOIN_ID: #{$chatroom_details["client_id"]}"
          #puts template_msg
          @server.puts( template_msg )
          
          run

        else
          template_msg = "CHAT: #{$chatroom_details["chatroom_ref"]}\nJOIN_ID: #{$chatroom_details["client_id"]}\nCLIENT_NAME: #{$chatroom_details["client_name"]}\nMESSAGE: #{msg}\n\n"
          #puts template_msg
          @server.puts( template_msg )
        end
        
        #puts template_msg
        
      }
    end
  end
end
 
hostname = "localhost"
portnumber = 2000

# Providing Command-line Arguments
ARGV.each do|shell_argument|
  # Abort program if the user tries to provide more than 1 port number
  if (ARGV.size > 1)
    abort("Enter only one port number \nstart.sh portnumber")

  # Replace default port number with the user input
  elsif (ARGV.size == 1)
  portnumber = shell_argument
  puts "Port number: #{shell_argument}" 
  end
end 

# Create login server
login_server = TCPSocket.open( hostname, portnumber )
#chatroom_server = TCPSocket.open( "localhost", chatroom_port  )

Client.new( login_server)#chatroom_server )
